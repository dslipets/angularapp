var bakeApp = angular.module('bakeApp', [
    'ui.router',
    'ngCookies',
    'ui.bootstrap',
    'ngTinyScrollbar',
    'pascalprecht.translate',
    'ngStorage',
    'configuration', 
    'ngAnimate',
    'ngMaterial',
    'loginModule',
    'categoryModule',
    'maincategoryModule',
    'ingredientsModule',
    'recipeModule',
    'managementModule',
    'customerModule',
    'cfp.loadingBar',
    'angular-loading-bar'
]);
bakeApp.directive('stringToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(value) {
                return '' + value;
            });
            ngModel.$formatters.push(function(value) {
                return parseFloat(value);
            });
        }
    };
});
bakeApp.service('authInterceptor',[ '$q', '$rootScope', '$localStorage', function($q, $rootScope, $localStorage) {
    var service = this;
    service.responseError = function(response) {
        if (response.status === 401){
            $rootScope.$state.go('Login');
            $rootScope.userAuth = false;
            $localStorage.userToken = undefined;
        } else{
        }
        return $q.reject(response);
    };
}]);
bakeApp.service('sessionInjector',[ '$q', '$rootScope', '$localStorage', function($q, $rootScope, $localStorage) {
    var sessionInjector = {
        request: function(config) {
            config.headers['Authorization'] = 'Bearer ' + $localStorage.userToken;
            return config;
        }
    };
    return sessionInjector;
}]);
bakeApp.service('initData', ['$http', 'config', function ($http, config){
    return {
        retrieveData : function() {
            return $http.get(config.backend + config.init);
        }
    }
}]);
bakeApp.run(['$rootScope', '$cookies', '$location', '$state', '$stateParams', 'checkAuth', '$localStorage', 'userService', 'initData', 'cfpLoadingBar',
    function($rootScope, $cookies, $location, $state, $stateParams, checkAuth, $localStorage, userService, initData, cfpLoadingBar) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.userAuth = false;
    if ($localStorage.userToken) {
        $rootScope.userAuth = true;
    }
    if($rootScope.userAuth = true){
        userService.get(function (data, status) {
            if (status === 200) {$rootScope.profileData = data.user; $rootScope.profilePackage = data.user.business.packageType; $rootScope.meBusiness = data.user.business;}
        });
        initData.retrieveData().then(function(data){
            $rootScope.initDish = data.data.bakingDishes;
            $rootScope.initAttributes = data.data.attributes;
            $rootScope.catImage = data.data.categoryImages;
            $rootScope.weigthUnitData = data.data.ingrediants.weightUnits;
            $rootScope.business = data.data.business.packages;
            $rootScope.language = data.data.languages.flatArray;
        });
    }
    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
            cfpLoadingBar.complete();
            $rootScope.closeAlert();
            $rootScope.myDynamicClass = '';
            checkAuth.checkAccess(event, toState, toParams, fromState, fromParams);
         }
    );
    $rootScope.logout = function(){
        $rootScope.userAuth = false;
        $localStorage.userToken = null;
        $rootScope.$state.go('Login')
    };
    $rootScope.sketchImage = {
            0: 'images/cake/01.svg',
            1: 'images/cake/02.svg',
            2: 'images/cake/03.svg',
            3: 'images/cake/04.svg',
            4: 'images/cake/05.svg',
            5: 'images/cake/06.svg',
            6: 'images/cake/07.svg',
            7: 'images/cake/08.svg',
            8: 'images/cake/09.svg'
    };
    $rootScope.successMessage = [];
    $rootScope.errorMessage = [];
    $rootScope.closeAlert = function (index) {
        $rootScope.successMessage.splice(index, 1);
        $rootScope.errorMessage.splice(index, 1);
    };
    $rootScope.$watch('successMessage',function (newVal) {
        if($rootScope.successMessage.length > 1){
            $rootScope.successMessage.splice(index, 1);
        }
    }, true);
    }]);