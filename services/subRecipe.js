categoryModule.service('subrecipeService', [
    '$http',
    '$rootScope',
    'config',
    function ($http, $rootScope, config) {

        this.get = function(data, callback) {
            $http.post(config.backend + config.subrecipe.search, data).success(callback).error(callback);
        };
        this.getsublist = function(data, callback) {
            $http.post(config.backend + config.subrecipe.recipelist, data).success(callback).error(callback);
        };
        this.add = function(data, callback) {
            $http.post(config.backend + config.subrecipe.list, data).success(callback).error(callback);
        };
        this.getAll = function(callback) {
            $http.get(config.backend + config.subrecipe.list, {ignoreLoadingBar: true}).success(callback).error(callback);
        };
        this.view = function(id, callback) {
            $http.get(config.backend + config.subrecipe.list + '/' + id).success(callback).error(callback);
        };
        this.addrecipe = function(data, callback) {
            $http.post(config.backend + config.subrecipe.add, data).success(callback).error(callback);
        };
        this.updatecat = function(id, data, callback) {
            $http.put(config.backend + config.subrecipe.list + '/' + id, data).success(callback).error(callback);
        };
        this.update = function(id, data, callback) {
            $http.put(config.backend + config.subrecipe.edit + '/' + id, data).success(callback).error(callback);
        };
        this.editrecipe = function(id, callback) {
            $http.get(config.backend + config.subrecipe.edit + '/' + id).success(callback).error(callback);
        };
        this.delete = function(id, callback) {
            $http.delete(config.backend + config.subrecipe.list + '/' + id).success(callback).error(callback);
        };
        this.search = function(data, callback) {
            $http.post(config.backend + config.subrecipe.search, data).success(callback).error(callback);
        };
        this.searchsubrecipe = function(data, callback) {
            $http.post(config.backend + config.subrecipe.recipelist, data).success(callback).error(callback);
        };
        this.categoryShare = function(id, data, callback) {
            $http.post(config.backend + config.subrecipe.cat_share + id, data).success(callback).error(callback);
        };
        this.recipeShare = function(id, data, callback) {
            $http.post(config.backend + config.subrecipe.share + id, data).success(callback).error(callback);
        };
    }]);