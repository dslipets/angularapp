managementModule.service('userService', [
    '$http',
    '$rootScope',
    'config',
    function ($http, $rootScope, config) {
        this.get = function(callback) {
            $http.get(config.backend + config.users.me).success(callback).error(callback);
        };
        this.view = function(id, callback) {
            $http.get(config.backend + config.users.me + '/' + id).success(callback).error(callback);
        };
        this.add = function(data, callback) {
            $http.post(config.backend + config.users.business, data).success(callback).error(callback);
        };
        this.update = function(data, callback) {
            $http.put(config.backend + config.users.profile, data).success(callback).error(callback);
        };
        this.delete = function(id, callback) {
            $http.delete(config.backend + config.users.me + '/' + id).success(callback).error(callback);
        };
        this.buy = function(id, callback) {
            $http.get(config.backend + config.users.payments + '/' + id).success(callback).error(callback);
        };
    }]);