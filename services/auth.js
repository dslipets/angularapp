loginModule.factory('loginService', [
    '$http',
    '$rootScope',
    'config',
    '$cookies',
    '$location',
    '$localStorage',
    function ($http, $rootScope, config, $cookies, $location, $localStorage) {
        return function(data, callback) {
            var success = function(res) {
                $rootScope.userToken = res.data.token;
                $rootScope.error = false;
                $localStorage.userToken = res.data.token;
                $location.path('/');
                return callback(true, res);
            };
            var error = function(res) {
                $rootScope.userToken = null;
                $rootScope.error = true;
                $localStorage.userToken = undefined;
                $location.path('/login');
                return callback(false, res);
            };
            $http.post(config.backend + config.url.auth, data).then(success, error);

        };
}]);
loginModule.service('checkAuth', [
    '$injector',
    function($injector) {
        this.checkAccess = function(event, toState, toParams, fromState, fromParams) {
            var $scope = $injector.get('$rootScope'),
                $localStorage = $injector.get('$localStorage');

                if (toState.data !== undefined) {
                    if (toState.data.noLogin !== undefined && toState.data.noLogin) {

                    }
                } else {
                    if (!$localStorage.userToken) {
                        event.preventDefault();
                        $scope.userAuth = false;
                    } else {
                        $scope.userAuth = true;
                    }
                }


        };
    }]);