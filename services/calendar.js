customerModule.service('calendarService', [
    '$http',
    '$rootScope',
    'config',
    function ($http, $rootScope, config) {
        this.get = function(data, callback) {
            $http.post(config.backend + config.calendar.search, data).success(callback).error(callback);
        };
        this.view = function(id, callback) {
            $http.get(config.backend + config.calendar.crud + '/' + id).success(callback).error(callback);
        };
        this.add = function(data, callback) {
            $http.post(config.backend + config.calendar.crud, data).success(callback).error(callback);
        };
        this.update = function(id, data, callback) {
            $http.put(config.backend + config.calendar.crud + '/' + id, data).success(callback).error(callback);
        };
        this.delete = function(id, callback) {
            $http.delete(config.backend + config.calendar.crud + '/' + id).success(callback).error(callback);
        };
        this.search = function(data, callback) {
            $http.post(config.backend + config.calendar.search, data).success(callback).error(callback);
        };
    }]);