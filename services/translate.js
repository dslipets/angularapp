var translates = {
    kg: 'קילוגרם',
    libra: "ליברה",
    liter: "ליטר",
    oz: "משהו",
    units: "יחידות",
    rectangle: 'מלבן',
    round: 'עגולה',
    milky: 'חלבי',
    vegan: 'טבעוני',
    vegetarian: 'צמחוני',
    parve: 'פרווה',
    gluten_free: 'ללא גלוטן',
    sugar_free: 'ללא סוכר',
    auth : {
        title: 'התחברות',
        email: 'שם משתמש',
        password: 'סיסמה',
        btnLogin: 'התחבר'
    },
    customer: {
        form: {
            inputName: 'שם הלקוח',
            inputBussinessNubmer: 'מספר עוסק',
            inputPhone: 'טלפון',
            inputEmail: 'אימייל',
            inputAddress: 'כתובת',
            inputCity: 'עיר',
            inputDate: 'תאריך חשוב',
            inputDescription: 'תיאור'
        },
        create: {
            title: 'הוסף לקוח',
            btnCreate: 'הוסף לקוח'
        },
        edit: {
            title: 'ערוך לקוח',
            btnCreate: 'שמור'
        },
        view: {
            title: 'הוסף לקוח',
            inputSearch: 'חיפוש',
            btnCreate: 'הוסף לקוח',
            table: {
                name: 'שם הלקוח',
                email: 'אימייל',
                phone: 'טלפון',
                address: 'כתובת',
                actions: 'פעולות'
            }
        }
    },
    ingredients: {
        form:{
            name: 'שם חומר הגלם',
            weight: 'יחידת משקל',
            amount: 'כמות',
            price: 'מחיר'
        },
        create: {
            title: 'הוספת חומר גלם',
            btnCreate: 'שמור חומר גלם'
        },
        list: {
            btnCreate: 'הוסף חדש',
            inputSearch: 'חיפוש',
            table:{
                name: 'שם חומר הגלם',
                weight: 'משקל',
                weightUnit: 'יחידת משקל',
                price: 'מחיר ליחידת משקל',
                actions: 'פעולות'
            }
        }
    },
    management: {
        view: {
            title: 'הגדרות העסק',
            purchase: 'שדרוג/רכישת מנוי',
            btnPurchase: 'שוכר',
            btnSave : 'שמור',
            form: {
                upload: 'העלאת תמונה',
                name: 'שם העסק',
                address: 'כתובת',
                city: 'עיר',
                language: 'שפת מערכת',
                email: 'אימייל',
                phone: 'טלפון',
                cellPhone: 'פלאפון'
            },
            subscription: {
                title: 'מנוי',
                date: 'תאריך רכישה',
                package: 'מסלול',
                billing: 'מחזור חיוב',
                expires: 'בתוקף עד'
            }
        }
    },
    recipe: {
        form: {
            name: 'שם המתכון',
            temperature: 'טמפרטורת אפיה',
            time: 'זמן הכנה',
            category: 'משפחה'
        },
        draw: {
            open: 'פתח לוח שרטוט',
            save: 'שמור',
            close: 'איפוס',
            select: 'בחירה'
        },
        dish: {
            assembly: 'הרכבה',
            title: 'תבנית',
            category: 'קטגוריה',
            ingredients: 'חומרי גלם',
            instructions: 'הוראות',
            notes: 'הערות',
            checkbox: 'הוסף קו',
            remove: 'פעולות',
            btnAdd: 'צור חדש',
            btnEdit: 'שמור מתכון',
            btnCreate: 'צור מתכון'
        },
        main: {
            create: {
                title : 'יצירת מתכון'
            },
            edit: {
                title : 'יצירת מתכון',
                back: 'go back to category'
            },
            category: {
                create: {
                    title: 'הוסף קטגוריה',
                    inputTitle: 'הכנס שם לקטגוריה',
                    btnCreate: 'צור קטגוריה'
                },
                edit: {
                    title: 'ערוך קטגוריה',
                    inputTitle: 'ערוך קטגוריה',
                    btnEdit: 'שמור'
                },
                list: {
                    btnCreate: 'צור מתכון',
                    inputSearch: 'חיפוש',
                    table: {
                        name: 'שם תת המתכון',
                        date: 'תאריך יצירה',
                        rating: 'דירוג',
                        value: 'סימון תזונתי',
                        createdBy: 'נוצר ע״י',
                        actions: 'פעולות'
                    },
                    nocontent: 'Sorry no data loading'
                },
                share: {
                    title: 'שיתוף קטגוריה',
                    inputEmail: 'שיתוף קטגוריה',
                    btnShare: 'שתף קטגוריה'
                },
                view: {
                    btnCreate: 'צור משפחה',
                    inputSearch: 'חיפוש'
                }
            }
        },
        sub: {
            create: {
                title: 'יצירת תת מתכון'
            },
            edit: {
                title: 'עריכת תת מתכון',
                back: 'go back to category'
            },
            category: {
                create: {
                    title: 'הוסף קטגוריה',
                    inputTitle: 'הכנס שם לקטגוריה',
                    btnCreate: 'צור קטגוריה'
                },
                edit: {
                    title: 'ערוך קטגוריה',
                    inputTitle: 'ערוך קטגוריה',
                    btnEdit: 'שמור'
                },
                list: {
                    btnCreate: 'צור תת מתכון',
                    inputSearch: 'חיפוש',
                    table: {
                        name: 'שם תת המתכון',
                        date: 'תאריך יצירה',
                        rating: 'דירוג',
                        value: 'סימון תזונתי',
                        createdBy: 'נוצר ע״י',
                        actions: 'פעולות'
                    },
                    nocontent: 'Sorry no data loading'
                },
                share: {
                    title: 'שיתוף קטגוריה',
                    inputEmail: 'שיתוף קטגוריה',
                    btnShare: 'שתף קטגוריה'
                },
                view: {
                    btnCreate: 'צור קטגוריה',
                    inputSearch: 'חיפוש'
                }
            }
        }
    },
    window: {
        error: '?האם אתה בטוח',
        helped_error: 'לאחר פעולה זאת, תת המתכון הנבחר',
        helped_error_strong: 'ימחק לאלתר',
        btn_yes: 'מחק',
        btn_no: 'בטל'
    },
    calendar: {
        page_title: 'יצירת הזמנה',
        btn_create: 'צור הזמנה',
        create: {
            title: 'יצירת הזמנה',
            name: 'שם הלקוח',
            date: 'תאריך ההזמנה',
            notes: 'תיאור ההזמנה',
            amount: 'סכום תשלום',
            btnCreate: 'צור הזמנה'
        },
        edit: {
            title: 'עריכת הזמנה',
            name: 'שם הלקוח',
            date: 'תאריך ההזמנה',
            notes: 'תיאור ההזמנה',
            amount: 'סכום תשלום',
            btnCreate: 'שמור הזמנה'
        }
    },
    calculation: {
        page_title: 'חישוב עלויות',
        btn_calc: 'חשב עלות',
        list_title: 'עלויות',
        form: {
            category: 'משפחה',
            recipe: 'שם המתכון',
            quantity: 'כמות',
            duration: 'זמן הכנה',
            hour: 'עלות לשעת עבודה'
        }
    }
};