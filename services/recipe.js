categoryModule.service('recipeService', [
    '$http',
    '$rootScope',
    'config',
    function ($http, $rootScope, config) {

        this.get = function(data, callback) {
            $http.post(config.backend + config.recipe.search, data).success(callback).error(callback);
        };
        this.getsublist = function(data, callback) {
            $http.post(config.backend + config.recipe.recipelist, data).success(callback).error(callback);
        };
        this.add = function(data, callback) {
            $http.post(config.backend + config.recipe.list, data).success(callback).error(callback);
        };
        this.getAll = function(callback) {
            $http.get(config.backend + config.recipe.list, {ignoreLoadingBar: true}).success(callback).error(callback);
        };
        this.view = function(id, callback) {
            $http.get(config.backend + config.recipe.list + '/' + id).success(callback).error(callback);
        };
        this.addrecipe = function(data, callback) {
            $http.post(config.backend + config.recipe.add, data).success(callback).error(callback);
        };
        this.update = function(id, data, callback) {
            $http.put(config.backend + config.recipe.list + '/' + id, data).success(callback).error(callback);
        };
        this.updaterecipe = function(id, data, callback) {
            $http.put(config.backend + config.recipe.edit + '/' + id, data).success(callback).error(callback);
        };
        this.editrecipe = function(id, callback) {
            $http.get(config.backend + config.recipe.edit + '/' + id).success(callback).error(callback);
        };
        this.delete = function(id, callback) {
            $http.delete(config.backend + config.recipe.list + '/' + id).success(callback).error(callback);
        };
        this.search = function(data, callback) {
            $http.post(config.backend + config.recipe.search, data).success(callback).error(callback);
        };
        this.searchrecipe = function(data, callback) {
            $http.post(config.backend + config.recipe.recipelist, data).success(callback).error(callback);
        };

        this.categoryShare = function(id, data, callback) {
            $http.post(config.backend + config.recipe.cat_share + id, data).success(callback).error(callback);
        };
        this.recipeShare = function(id, data, callback) {
            $http.post(config.backend + config.recipe.share + id, data).success(callback).error(callback);
        };
    }]);