bakeApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$mdThemingProvider', '$translateProvider', '$httpProvider', 'scrollbarProvider', 'cfpLoadingBarProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider, $mdThemingProvider, $translateProvider, $httpProvider, scrollbarProvider, cfpLoadingBarProvider) {
        cfpLoadingBarProvider.parentSelector = '#loading-wrapper';
        cfpLoadingBarProvider.spinnerTemplate = '<div class="cssload-preload-juggle"><div class="cssload-ball"></div><div class="cssload-ball"></div><div class="cssload-ball"></div></div>';
        scrollbarProvider.defaults.autoUpdate = true;
        $mdThemingProvider.definePalette('amazingPaletteName', {
            '50': 'c2b7af',
            '100': 'c2b7af',
            '200': 'c2b7af',
            '300': 'c2b7af',
            '400': 'c2b7af',
            '500': 'c2b7af',
            '600': 'c2b7af',
            '700': 'c2b7af',
            '800': '6a655d',
            '900': '6a655d',
            'A100': '6a655d',
            'A200': '6a655d',
            'A400': '6a655d',
            'A700': '6a655d',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': ['50', '100',
                '200', '300', '400', 'A100'],
            'contrastLightColors': undefined
        });
        $mdThemingProvider.definePalette('lgrey', {
            '50': '6a655d',
            '100': '6a655d',
            '200': '6a655d',
            '300': '6a655d',
            '400': '6a655d',
            '500': '6a655d',
            '600': '6a655d',
            '700': '6a655d',
            '800': '6a655d',
            '900': '6a655d',
            'A100': '6a655d',
            'A200': '6a655d',
            'A400': '6a655d',
            'A700': '6a655d',
            'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                                // on this palette should be dark or light
            'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
                '200', '300', '400', 'A100'],
            'contrastLightColors': undefined    // could also specify this if default was 'dark'
        });
        $mdThemingProvider.theme('default')
            .primaryPalette('lgrey')
            .accentPalette('amazingPaletteName');
        $httpProvider.interceptors.push('authInterceptor');
        $httpProvider.interceptors.push('sessionInjector');
        $translateProvider
            .translations('he', translates)
            .preferredLanguage('he');
        $translateProvider.useSanitizeValueStrategy('escape');

        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise("/login");
        $stateProvider
            .state('Login', {
                url: "/login",
                data: {
                    'noLogin': true
                },
                views: {
                    'header': {
                        templateUrl: 'views/blocks/headerAuth.html'
                    },
                    'content': {
                        templateUrl: 'views/auth/login.html',
                        controller: 'accessLoginController'
                    }
                }
            })
            .state('Dashboard', {
                url: "/",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/dashboard/view.html',
                        controller: function($rootScope, userService, initData){
                            $rootScope.myDynamicClass = 'dashboard';
                            if($rootScope.profileData === undefined || $rootScope.profileData === false || $rootScope.profileData === null){
                                userService.get(function (data, status) {
                                    if (status === 200) {$rootScope.profileData = data.user;}
                                });
                                initData.retrieveData().then(function(data){
                                    $rootScope.initDish = data.data.bakingDishes;
                                    $rootScope.initAttributes = data.data.attributes;
                                    $rootScope.business = data.data.business.packages;
                                    $rootScope.language = data.data.languages.flatArray;
                                });
                            }
                        }
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('CategoryMain', {
                url: "/category",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/main/category/view.html',
                        controller: 'maincategoryController'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('CategoryRecipe', {
                url: "/category/recipe/:id",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/main/category/list.html',
                        controller: 'mainrecipeList'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('Category', {
                url: "/category/sub",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/sub/category/view.html',
                        controller: 'categoryController'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('CategorySub', {
                url: "/category/subrecipe/:id",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/sub/category/list.html',
                        controller: 'recipeList'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('Ingredients', {
                url: "/ingredients",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/ingredients/list.html',
                        controller: 'ingredientsController'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('Management', {
                url: "/management",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/management/view.html',
                        controller: 'managementCtrl'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('RecipeCalculation', {
                url: "/recipe/calculation",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/main/calculation.html',
                        controller: 'calcRecipeCtrl'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('RecipeView', {
                url: "/recipe/:id",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/main/view.html',
                        controller: 'viewRecipeCtrl'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('RecipeEdit', {
                url: "/recipe/edit/:id",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/main/edit.html',
                        controller: 'editRecipeCtrl'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('Recipe', {
                url: "/main/recipe/create",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/main/create.html',
                        controller: 'createRecipeCtrl'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('SubRecipe', {
                url: "/subrecipe/create",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/sub/sub.create.html',
                        controller: 'recipeCtrl'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('SubRecipeView', {
                url: "/subrecipe/:id",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/sub/view.html',
                        controller: 'viewSubRecipeCtrl'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('SubRecipeEdit', {
                url: "/subrecipe/edit/:id",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/recipe/sub/sub.edit.html',
                        controller: 'editrecipeCtrl'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('Customer', {
                url: "/customer",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/customer/customer/view.html',
                        controller: 'customerController'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('Calendar', {
                url: "/calendar",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/customer/calendar/view.html',
                        controller: 'calendarController'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
            .state('Calculation', {
                url: "/calculation",
                views: {
                    'header': {
                        templateUrl: 'views/blocks/header.html'
                    },
                    'content': {
                        templateUrl: 'views/customer/calculation/view.html',
                        controller: 'calculationController'
                    },
                    'navigation': {
                        templateUrl: 'views/blocks/navigation.html'
                    }
                }
            })
    }]);