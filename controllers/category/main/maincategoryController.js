var maincategoryModule = angular.module('maincategoryModule', []);

maincategoryModule.controller('maincategoryController', ['$rootScope', '$scope', '$mdDialog', 'recipeService', 'config', '$timeout',
    function($rootScope, $scope, $mdDialog, recipeService, config, $timeout) {
        $scope.recipeimageurl = (config.backend + 'images/categories/');
        $scope.ingredients = null;
        $scope.currentPage = 1;
        var offset = 0;
        var list = function(offset) {
            offset = {
                page: ($scope.currentPage - 1)
            };
            recipeService.get(offset, function (data, status) {
                if (status === 200) {
                    $timeout(function () {
                        $scope.category = data.data;
                    }, 600);
                    $scope.totalItems = data.total;
                    $scope.itemsPerPage = data.resultsPerPage;
                    $scope.maxSize = data.resultsPerPage;
                    if($scope.itemsPerPage < $scope.totalItems){
                        $scope.show = true;
                    }
                } else{
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };

        $scope.$watch('currentPage', function() {
            list(offset);
        });

        $scope.searchItem = function() {
            var s_data = {
                phrase: $scope.searchText
            };
            recipeService.search(s_data, function (data, status) {
                if (status === 200) {
                    $timeout(function () {
                        $scope.category = data.data;
                    }, 600);
                    $scope.totalItems = data.total;
                    $scope.itemsPerPage = data.resultsPerPage;
                    $scope.maxSize = data.resultsPerPage;
                    $scope.show = $scope.itemsPerPage < $scope.totalItems;
                } else {

                }
            });
        };
        $scope.deleteDialog= function (id, ev, index) {
            $rootScope.successShow = false;
            $rootScope.errorShow = false;
            $mdDialog.show({
                templateUrl: 'views/blocks/delete.tmpl.html',
                clickOutsideToClose: true,
                targetEvent: ev,
                controller: function ($scope, $mdDialog) {
                    $scope.remove = function(remove) {
                        $mdDialog.hide(remove);
                    };
                    $scope.close = function() {
                        $mdDialog.cancel();
                    };
                }
            }).then(function(remove) {
                recipeService.delete(id, function (data, status) {
                    if (status === 200) {
                        $scope.category.splice(index, 1);
                        $mdDialog.cancel();
                    } else {}
                });
            }, function() {
            });
        };
        $scope.edit = function (id) {
            $rootScope.closeAlert();
            $mdDialog.show({
                locals:{itemId: id},
                controller: 'mainCategoryEditCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/recipe/main/category/edit.tmpl.html'
            }).then(function() {
                list();
            });
        };
        $scope.showCreate = function() {
            $rootScope.closeAlert();
            $mdDialog.show({
                controller: 'mainCategoryCreateCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/recipe/main/category/create.tmpl.html'
            }).then(function() {
                list();
            });
        };
        $scope.showShare = function(id) {
            $rootScope.closeAlert();
            $mdDialog.show({
                locals: {itemId: id},
                controller: 'MainShareCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/recipe/main/category/share.tmpl.html'
            });
        };
}]);
