maincategoryModule.controller('mainrecipeList', ['$rootScope', '$scope', '$mdDialog',
    'recipeService', 'initData', '$stateParams', 'config', '$timeout',
    function($rootScope, $scope, $mdDialog, recipeService, initData, $stateParams, config, $timeout) {
        $scope.ingredients = null;
        $scope.currentPage = 1;
        var offset = null;
        $scope.recipeimageurl = (config.backend + 'images/categories/');
        $scope.contentLoaded = false;
        $scope.noContent = false;
        var list = function(offset) {
            offset ={
                page: ($scope.currentPage - 1),
                recipeCategory: ($stateParams.id),
                selectFilters : {
                    _id: 1,
                    attributes: 1,
                    business: 1,
                    creationDate: 1,
                    rating: 1,
                    title: 1,
                    isShared: 1
                }
            };
            recipeService.getsublist(offset, function (data, status) {
                if (status === 200) {
                    $timeout(function () {
                        $scope.category = data.data;
                    }, 600);
                    $scope.categoryData = data.recipeCategory;
                    $scope.totalItems = data.total;
                    $scope.itemsPerPage = data.resultsPerPage;
                    $scope.maxSize = data.resultsPerPage;
                    if($scope.itemsPerPage < $scope.totalItems){
                        $scope.show = true;
                    }
                    if(data.total){
                        $scope.contentLoaded = true;
                    } else{
                        $scope.noContent = true;
                    }
                } else{
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };

        $scope.$watch('currentPage', function() {
            list(offset);
        });

        $scope.searchItem = function() {
            var s_data = {
                phrase: $scope.searchText,
                recipeCategory: ($stateParams.id),
                selectFilters : {
                    _id: 1,
                    attributes: 1,
                    business: 1,
                    creationDate: 1,
                    rating: 1,
                    title: 1,
                    isShared: 1
                }
            };
            recipeService.searchsubrecipe(s_data, function (data, status) {
                if (status === 200) {
                    $scope.category = data.data;
                    $scope.totalItems = data.total;
                    $scope.itemsPerPage = data.resultsPerPage;
                    $scope.maxSize = data.resultsPerPage;
                    $scope.show = $scope.itemsPerPage < $scope.totalItems;
                    if(data.total){
                        $scope.contentLoaded = true;
                    } else{
                        $scope.noContent = true;
                    }
                } else {

                }
            });
        };
        $scope.showShare = function(id) {
            $rootScope.closeAlert();
            $mdDialog.show({
                locals: {itemId: id},
                controller: 'MainRecipeShareCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/recipe/main/category/share.tmpl.html'
            });
        };
        $scope.showCreate = function() {
            $rootScope.closeAlert();
            $mdDialog.show({
                controller: 'CategoryCreateCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/recipe/main/category/create.tmpl.html'
            }).then(function() {
                list();
            });
        };
        $scope.deleteDialog= function (id, ev, index) {
            $rootScope.successShow = false;
            $rootScope.errorShow = false;
            $mdDialog.show({
                templateUrl: 'views/blocks/delete.tmpl.html',
                clickOutsideToClose: true,
                targetEvent: ev,
                controller: function ($scope, $mdDialog) {
                    $scope.remove = function(remove) {
                        $mdDialog.hide(remove);
                    };
                    $scope.close = function() {
                        $mdDialog.cancel();
                    };
                }
            }).then(function(remove) {
                recipeService.delete(id, function (data, status) {
                    if (status === 204) {
                        $scope.category.splice(index, 1);
                        $mdDialog.cancel();
                    } else {}
                });
            }, function() {
            });
        };
    }]);