maincategoryModule.controller('mainCategoryEditCtrl', ['$rootScope', '$scope', '$mdDialog', 'itemId', 'recipeService', 'initData', 'config',
    function($rootScope, $scope, $mdDialog, itemId, recipeService, initData, config) {
        $scope.recipeimageurl = (config.backend + 'images/categories/');
        $scope.image = $rootScope.catImage;
        $scope.errorMessage = [];
        $scope.close = function() {
            $mdDialog.cancel();
        };
        recipeService.view(itemId, function (data, status) {
            if (status === 200) {
                $scope.category = data.data;
            } else {
            }
        });
        $scope.edit = function () {
            recipeService.update(itemId, $scope.category, function (data, status) {
                if (status === 200) {
                    $rootScope.successMessage.push(data);
                    $mdDialog.hide();
                } else {
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };
        $scope.closeAlert = function (index) {
            $scope.errorMessage.splice(index, 1);
        };
    }]);