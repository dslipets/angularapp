maincategoryModule.controller('mainCategoryCreateCtrl', ['$rootScope', '$scope', '$mdDialog', 'initData', 'recipeService', 'config',
    function($rootScope, $scope, $mdDialog, initData, recipeService, config) {
        $scope.recipeimageurl = (config.backend + 'images/categories/');
        $scope.image = $rootScope.catImage;
        $scope.errorMessage = [];
        $scope.close = function() {
            $mdDialog.cancel();
        };
        $scope.create = function () {
            recipeService.add($scope.category, function (data, status) {
                if (status === 200) {
                    $rootScope.successMessage.push(data);
                    $mdDialog.hide();
                } else {
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };
        $scope.closeAlert = function (index) {
            $scope.errorMessage.splice(index, 1);
        };
    }]);