categoryModule.controller('CategoryEditCtrl', ['$rootScope', '$scope', '$mdDialog', 'itemId', 'subrecipeService', 'initData', 'config',
    function($rootScope, $scope, $mdDialog, itemId, subrecipeService, initData, config) {
        $scope.recipeimageurl = (config.backend + 'images/categories/');
        $scope.image = $rootScope.catImage;
        $scope.errorMessage = [];
        $scope.close = function() {
            $mdDialog.cancel();
        };
        subrecipeService.view(itemId, function (data, status) {
            if (status === 200) {
                $scope.category = data.data;
            } else {
            }
        });
        $scope.create = function () {
            subrecipeService.updatecat(itemId, $scope.category, function (data, status) {
                if (status === 200) {
                    $rootScope.successMessage.push(data);
                    $mdDialog.hide();
                } else {
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };
        $scope.closeAlert = function (index) {
            $scope.errorMessage.splice(index, 1);
        };
    }]);