categoryModule.controller('SubShareCtrl', ['$rootScope', '$scope', '$mdDialog', 'subrecipeService', 'itemId',
    function($rootScope, $scope, $mdDialog, subrecipeService, itemId) {
        $scope.emails = {};
        $scope.emailsList = null;
        $scope.errorMessage = [];
        $scope.close = function() {
            $mdDialog.cancel();
        };
        $scope.share = function () {
            if($scope.emailsList !== null){
                $scope.emails = $scope.emailsList.split(',').join().replace(/\s+/g, '').split(',', 20);
            }
            var emailsData = {
                emails: $scope.emails
            };
            subrecipeService.categoryShare(itemId, emailsData, function (data, status) {
                if (status === 200) {
                    $mdDialog.hide();
                    $rootScope.successMessage.push(data);
                } else {
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };
    }]);
categoryModule.controller('SubRecipeShareCtrl', ['$rootScope', '$scope', '$mdDialog', 'subrecipeService', 'itemId',
    function($rootScope, $scope, $mdDialog, subrecipeService, itemId) {
        $scope.emails = {};
        $scope.emailsList = null;
        $scope.close = function() {
            $mdDialog.cancel();
        };
        $scope.share = function () {
            if($scope.emailsList !== null){
                $scope.emails = $scope.emailsList.split(',');
            }
            var emailsData = {
                emails: $scope.emails
            };
            subrecipeService.recipeShare(itemId, emailsData, function (data, status) {
                if (status === 200) {
                    $mdDialog.hide();
                    $rootScope.successMessage.push(data);
                } else {
                    $rootScope.errorMessage.push(data);
                    $scope.errorList = $rootScope.errorMessage.shift();
                }
            });
        };
    }]);