var ingredientsModule = angular.module('ingredientsModule', []);
ingredientsModule.controller('ingredientsController', ['$rootScope', '$scope', '$mdDialog', 'ingredientsService', '$timeout',
    function($rootScope, $scope, $mdDialog, ingredientsService, $timeout){
    $scope.ingredients = null;
    $scope.currentPage = 1;
    var offset = 0;
    var list = function(offset) {
        offset = {
            page: ($scope.currentPage - 1)
        };
        ingredientsService.get(offset, function (data, status) {
            if (status === 200) {
                $timeout(function () {
                    $scope.ingredients = data.data;
                }, 600);
                $scope.totalItems = data.total;
                $scope.itemsPerPage = data.resultsPerPage;
                $scope.maxSize = data.resultsPerPage;
                if($scope.itemsPerPage < $scope.totalItems){
                    $scope.show = true;
                }
            } else{
                $rootScope.errorMessage.push(data);
            }
        });
    };

    $scope.$watch('currentPage', function() {
        list(offset);
    });

    $scope.searchItem = function() {
        var s_data = {
            phrase: $scope.searchText
        };
        ingredientsService.search(s_data, function (data, status) {
            if (status === 200) {
                $timeout(function () {
                    $scope.ingredients = data.data;
                }, 600);
                $scope.totalItems = data.total;
                $scope.itemsPerPage = data.resultsPerPage;
                $scope.maxSize = data.resultsPerPage;
                $scope.show = $scope.itemsPerPage < $scope.totalItems;
            } else {}
        });
    };
    $scope.deleteDialog= function (id, ev, index) {
        $mdDialog.show({
            templateUrl: 'views/blocks/delete.tmpl.html',
            clickOutsideToClose: true,
            targetEvent: ev,
            controller: function ($scope, $mdDialog) {
                $scope.successShow = false;
                $scope.errorShow = false;
                $scope.remove = function(remove) {
                    $mdDialog.hide(remove);
                };
                $scope.close = function() {
                    $mdDialog.cancel();
                };
            }
        }).then(function(remove) {
            ingredientsService.delete(id, function (data, status) {
                if (status === 200) {
                    $scope.ingredients.splice(index, 1);
                    $rootScope.successShow = true;
                    $mdDialog.cancel();
                } else {}
            });
        }, function() {
        });
    };

    $scope.edit = function (item) {
        $rootScope.closeAlert();
        $mdDialog.show({
            locals:{editItem: item},
            controller: 'ingredientsEditCtrl',
            clickOutsideToClose : true,
            templateUrl: 'views/ingredients/edit.tmpl.html'
        }).then(function() {
            list();
        });
    };
    $scope.showCreate = function() {
        $mdDialog.show({
            controller: 'ingredientsCreateCtrl',
            clickOutsideToClose : true,
            templateUrl: 'views/ingredients/create.tmpl.html'
        }).then(function() {
            list();
        });
    };
}]);