ingredientsModule.controller('ingredientsEditCtrl', ['$rootScope', '$scope', 'ingredientsService', '$mdDialog', 'editItem', 'initData',
    function($rootScope, $scope, ingredientsService, $mdDialog, editItem, initData) {
        $scope.errorMessage = [];
        initData.retrieveData().then(function(data){
            $scope.weightUnit = data.data.ingrediants.weightUnits;
        });
        $scope.close = function() {
            $mdDialog.cancel();
        };
        $scope.ingredients = editItem;
        $scope.controls = function () {
            ingredientsService.update(editItem._id, $scope.ingredients, function (data, status) {
                if (status === 200) {
                    $rootScope.successMessage.push(data);
                    $mdDialog.hide();
                } else {
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };
        $scope.closeAlert = function (index) {
            $scope.errorMessage.splice(index, 1);
        };
    }]);