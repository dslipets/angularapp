ingredientsModule.controller('ingredientsCreateCtrl', ['$rootScope', '$scope', '$mdDialog', 'ingredientsService',
    function($rootScope, $scope, $mdDialog, ingredientsService) {
        $scope.close = function() {
            $mdDialog.cancel();
        };
        $scope.errorMessage = [];
        $scope.weightUnit = angular.copy($rootScope.weigthUnitData);
        $scope.controls = function () {
            ingredientsService.add($scope.ingredients, function (data, status) {
                if (status === 200) {
                    $rootScope.successMessage.push(data);
                    $mdDialog.hide();
                } else {
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };
        $scope.closeAlert = function (index) {
            $scope.errorMessage.splice(index, 1);
        };
    }]);
