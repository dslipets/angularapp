var managementModule = angular.module('managementModule', ['ngFileUpload']);

managementModule.controller('managementCtrl', ['$rootScope', '$scope', 'userService', 'initData', '$window', 'Upload',
    function($rootScope, $scope, userService, initData, $window, Upload){
        $scope.save = function () {
            $scope.profileData.business.picture = $scope.imageUrl;
            userService.update($scope.profileData, function (data, status) {
                if (status === 200) {
                    $rootScope.successMessage.push(data);
                    $scope.business = angular.copy($rootScope.business);
                    $scope.language = angular.copy($rootScope.language);
                } else {
                    $rootScope.errorMessage.push(data);
                }
            });
        };

        $scope.buy = function (value) {
            userService.buy(value, function (data, status) {
                if (status === 200) {
                    $scope.buy_url = data.responseObj.url;
                    $window.location.href = $scope.buy_url;
                } else {$scope.errors = data;}
            });
        };
        $scope.$watch('file', function () {
            Upload.base64DataUrl($scope.file).then(function(url){
                $scope.imageUrl = url;
            });
        });
}]);