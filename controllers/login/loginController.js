var loginModule = angular.module('loginModule', []);

loginModule.controller('accessLoginController', ['$rootScope', '$scope', '$window', 'loginService', '$state', function($rootScope, $scope, $window, loginService, $state ) {

    $scope.email='';
    $scope.password='';
    $scope.newUser={};
    $scope.login = function() {
        loginService({email: $scope.email, password: $scope.password}, function(success, data) {
            if(data.status === 401){
                $scope.errorMessage = data.data.message;
                $scope.errorShow = true;
            } else{
                if(($rootScope.userAuth = true) && $state.is('Login')){
                    $state.go('Dashboard');
                }
            }
        });
    };

}]);