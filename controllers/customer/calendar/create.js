customerModule.controller('calendarCreateCtrl', ['$rootScope', '$scope', '$mdDialog', 'calendarService', '$http', 'config',
    function($rootScope, $scope, $mdDialog, calendarService, $http, config) {
        $scope.event = {};
        $scope.errorMessage = [];
        $scope.close = function() {
            $mdDialog.cancel();
        };
        $scope.create = function () {
            calendarService.add($scope.event, function (data, status) {
                if (status === 200) {
                    $rootScope.successMessage.push(data);
                    $mdDialog.hide(data.data);
                } else {
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };
        $scope.selectItem = function (item) {
            $scope.event.title = item.title;
        };
        $scope.searchItems = function(searchText) {
            return $http.post(config.backend + config.customer.search, {phrase: searchText})
                .then(function(results) {
                    return results.data.data;
                });
        };
    }]);
