maincategoryModule.controller('calendarEditCtrl', ['$rootScope', '$scope', '$mdDialog', 'itemId', 'calendarService', '$filter',
    function($rootScope, $scope, $mdDialog, itemId, calendarService, $filter) {
        $scope.image = $rootScope.catImage;
        $scope.errorMessage = [];
        $scope.close = function() {
            $mdDialog.cancel();
        };
        $scope.StrToDate = function (str) {
            return new Date(str);
        };
        calendarService.view(itemId, function (data, status) {
            if (status === 200) {
                $scope.event = data.data;
                $scope.event.date = new Date(data.data.date);
            } else {
            }
        });
        $scope.edit = function () {
            calendarService.update(itemId, $scope.event, function (data, status) {
                if (status === 200) { $rootScope.successMessage = data.msg;
                    $rootScope.successShow = true;
                    $mdDialog.hide();
                } else {}
            });
        };
        $scope.closeAlert = function () {
            $scope.successShow = false;
            $scope.errorShow = false;
        }
    }]);