customerModule.controller('calendarController', ['$rootScope', '$scope', '$compile', 'uiCalendarConfig', '$timeout', '$mdDialog', 'calendarService',
    function($rootScope, $scope, $compile, uiCalendarConfig, $timeout, $mdDialog, calendarService) {
        var page = angular.element('.scroll-viewport');
        $scope.events = [];
        $scope.errorMessage = [];
        var list = function(params) {
            calendarService.get(params, function (data, status) {
                if (status === 200) {
                    $scope.events = [];
                    $scope.eventSources.splice(0,1);
                    angular.forEach(data.data, function (v, k) {
                        $scope.events[k] = {
                            id: data.data[k]._id,
                            title: data.data[k].title,
                            start: new Date(data.data[k].date),
                            allDay: false
                        };
                    });
                    $timeout(function () {
                        $scope.eventSources.push($scope.events);
                    }, 600);
                } else{
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };
        $scope.eventsF = function (start, jsEvent, view) {
            var s = new Date(start.start._d).getTime();
            var e = new Date(start.end._d).getTime();
            var params = {
                "startDate" : s,
                "endDate" : e
            };

            list(params);
            $scope.EndStarDate = angular.copy(params);
        };
        $scope.alertOnEventClick = function( data, jsEvent, view){
            $mdDialog.show({
                locals: {itemId: data.id},
                controller: 'calendarEditCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/customer/calendar/edit.tmpl.html'
            }).then(function() {
                list($scope.EndStarDate);
            });
        };
        $scope.changeView = function(view,calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
        };
        $scope.renderCalender = function(calendar) {
            var page = angular.element('.scroll-viewport');
            $timeout(function() {
                if(uiCalendarConfig.calendars[calendar]){
                    uiCalendarConfig.calendars[calendar].fullCalendar('render');
                }
            });
        };
        $scope.uiConfig = {
            calendar:{
                height: page.height() - 70,
                viewRender: $scope.eventsF,
                editable: true,
                header:{
                    left: 'next prev',
                    center: 'title',
                    right: 'month agendaWeek agendaDay'
                },
                isRTL: true,
                dayNamesShort: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת"],
                buttonText: {
                    month:    'חודש',
                    week:     'שבוע',
                    day:      'יום'
                },
                scrollTime: '00:00:00',
                eventLimit: true, // for all non-agenda views
                views: {
                    agenda: {
                        eventLimit: 4
                    }
                },
                eventClick: $scope.alertOnEventClick
            }
        };
        /* event sources array*/
        $scope.eventSources = [$scope.events];
        $scope.showCreate = function() {
            $mdDialog.show({
                controller: 'calendarCreateCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/customer/calendar/create.tmpl.html'
            }).then(function(data) {
                console.log(data);
                var newEvent = {
                    id: data._id,
                    title: data.title,
                    start: new Date(data.date),
                    allDay: false
                };
                console.log(newEvent);
                $scope.events.push(newEvent);
            });
        };
        $scope.closeAlert = function () {
            $scope.successShow = false;
            $scope.errorShow = false;
        };
    }]);