customerModule.controller('customersCreateCtrl', ['$rootScope', '$scope', '$mdDialog', 'customerService',
    function($rootScope, $scope, $mdDialog, customerService) {
        $scope.importantDates = [];
        $scope.customer = {};
        $scope.errorMessage = [];
        $scope.close = function() {
            $mdDialog.cancel();
        };
        $scope.controls = function () {
            $scope.customer.importantDates = $scope.importantDates;
            customerService.add($scope.customer, function (data, status) {
                if (status === 200) {
                    $rootScope.successMessage.push(data);
                    $mdDialog.hide();
                } else {
                    $scope.closeAlert();
                    $scope.errorMessage.push(data);
                }
            });
        };
        $scope.addrow = function (item) {
            var data = {
                date: $scope.date,
                description: $scope.description
            };
            $scope.importantDates.push(data);
        };
        $scope.addrow();
        $scope.remove = function (index) {
            $scope.importantDates.splice(index, 1);
        };
        $scope.closeAlert = function (index) {
            $scope.errorMessage.splice(index, 1);
        };
    }]);
