customerModule.controller('customersEditCtrl', ['$rootScope', '$scope', 'customerService', '$mdDialog', 'editItem',
    function($rootScope, $scope, customerService, $mdDialog, editItem) {
        $scope.close = function() {
            $mdDialog.hide();
        };
        $scope.StrToDate = function (str) {
            return new Date(str);
        };
        $scope.customer = editItem;
        $scope.importantDates = editItem.importantDates;
        $scope.controls = function () {
            customerService.update(editItem._id, $scope.customer, function (data, status) {
                if (status === 200) {
                    $rootScope.closeAlert();
                    $rootScope.successMessage.push(data);
                    $mdDialog.hide();
                } else {
                    $rootScope.errorMessage.push(data);
                }
            });
        };
        $scope.addrow = function (item) {
            var data = {
                date: $scope.date,
                description: $scope.description
            };
            $scope.importantDates.push(data);
        };
        $scope.remove = function (index) {
            $scope.importantDates.splice(index, 1);
        };
    }]);