customerModule.controller('customerController', ['$rootScope', '$scope', 'customerService', '$mdDialog', '$timeout',
    function($rootScope, $scope, customerService, $mdDialog, $timeout){
        $scope.customers = null;
        $scope.currentPage = 1;
        var offset = 0;
        var list = function(offset) {
            offset = {
                page: ($scope.currentPage - 1)
            };
            customerService.get(offset, function (data, status) {
                if (status === 200) {
                    $timeout(function () {
                        $scope.customers = data.data;
                    }, 600);
                    $scope.totalItems = data.total;
                    $scope.itemsPerPage = data.resultsPerPage;
                    $scope.maxSize = data.resultsPerPage;
                    if($scope.itemsPerPage < $scope.totalItems){
                        $scope.show = true;
                    }
                } else{
                    $rootScope.errorMessage.push(data);
                }
            });
        };

        $scope.$watch('currentPage', function() {
            list(offset);
        });

        $scope.searchItem = function() {
            var s_data = {
                phrase: $scope.searchText
            };
            customerService.search(s_data, function (data, status) {
                if (status === 200) {
                    $timeout(function () {
                        $scope.customers = data.data;
                    }, 600);
                    $scope.totalItems = data.total;
                    $scope.itemsPerPage = data.resultsPerPage;
                    $scope.maxSize = data.resultsPerPage;
                    $scope.show = $scope.itemsPerPage < $scope.totalItems;
                } else {}
            });
        };
        $scope.deleteDialog= function (id, ev, index) {
            $mdDialog.show({
                templateUrl: 'views/blocks/delete.tmpl.html',
                clickOutsideToClose: true,
                targetEvent: ev,
                controller: function ($scope, $mdDialog) {
                    $rootScope.successShow = false;
                    $rootScope.errorShow = false;
                    $scope.remove = function(remove) {
                        $mdDialog.hide(remove);
                    };
                    $scope.close = function() {
                        $mdDialog.cancel();
                    };
                }
            }).then(function(remove) {
                customerService.delete(id, function (data, status) {
                    if (status === 200) {
                        $scope.customers.splice(index, 1);
                        $rootScope.successShow = true;
                        $mdDialog.cancel();
                    } else {}
                });
            }, function() {
            });
        };

        $scope.edit = function (item) {
            $rootScope.closeAlert();
            $mdDialog.show({
                locals: {editItem: item},
                controller: 'customersEditCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/customer/customer/edit.tmpl.html'
            }).then(function() {
                list();
            });
        };
        $scope.showCreate = function() {
            $rootScope.closeAlert();
            $mdDialog.show({
                controller: 'customersCreateCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/customer/customer/create.tmpl.html'
            }).then(function() {
                list();
            });
        };
}]);