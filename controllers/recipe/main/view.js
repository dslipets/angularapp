recipeModule.controller('viewRecipeCtrl', ['$rootScope', '$scope', 'recipeService', 'Fabric', 'FabricConstants', '$stateParams', 'config',
    function($rootScope, $scope, recipeService, Fabric, FabricConstants, $stateParams, config) {
        $scope.imgUrl = config.uploads;
        $scope.fabric = {};
        $scope.FabricConstants = FabricConstants;
        $scope.multiplier = null;
        recipeService.editrecipe($stateParams.id, function (data, status) {
            if (status === 200) {
                $scope.recipe = data.data;
                $scope.fabric.loadJSON(data.data.sketch);
            recipeService.view(data.data.recipeCategory, function (data, status) {
                    if (status === 200) {
                        $scope.category = data.data;
                    } else {
                    }
                });
            } else{
                $scope.errorMessage = data;
            }
        });
        $scope.init = function() {
            $scope.fabric = new Fabric({});
        };
        $scope.$on('canvas:created', $scope.init);

    }]);