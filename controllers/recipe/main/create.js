recipeModule.controller('createRecipeCtrl', ['$rootScope', '$scope', '$mdDialog', 'Upload', 'subrecipeService',
    'recipeService', 'ingredientsService', '$http', 'initData', 'config', '$timeout', 'Fabric', 'FabricConstants', '$state', '$anchorScroll', 'cfpLoadingBar',
    function($rootScope, $scope, $mdDialog, Upload, subrecipeService, recipeService, ingredientsService, $http,
             initData, config, $timeout, Fabric, FabricConstants, $state, $anchorScroll, cfpLoadingBar) {
        $scope.recipe = {};
        $scope.recipeDynamicList = [];
        $scope.ingredientsList = [];
        $scope.ingredients = [];
        $scope.selectedDish = [];
        $scope.excludeItems = [];
        $scope.recipe.bakingDish= {};
        $scope.input = null;
        $scope.recipeimageurl = (config.backend + 'images/categories/');
        $scope.checkAttributes = [];
        $scope.listShow = false;
        $scope.$watch('file', function () {
            Upload.base64DataUrl($scope.file).then(function(url){
                $scope.imageUrl = url;
            });
        });
        $scope.clearImage = function() {
            $scope.file = null;
        };
        $scope.onChangeAttributes = function (v) {
            v.checkValue = !v.checkValue;
            if(v.checkValue){
                $scope.checkAttributes.push(v.value);
            } else {
                var index = $scope.checkAttributes.indexOf(v.value);
                $scope.checkAttributes.splice(index, 1);
            }
        };
        $scope.create = function () {
            angular.forEach($scope.recipeDynamicList, function (v, k) {
                $scope.recipeDynamicList[k].ingredients = $scope.recipeDynamicList[k].ingredients.filter(function (val) {
                    return val;
                });
            });
            $scope.recipe.picture = $scope.imageUrl;
            $scope.recipe.attributes = $scope.checkAttributes;
            if($scope.recipe.bakingDish.type !== undefined){
                $scope.recipe.bakingDish = $scope.selectedDish;
            }
            if($scope.fabric.json !== undefined){
                $scope.recipe.sketch = $scope.fabric.json;
            }
            $scope.recipe.subRecipes = $scope.recipeDynamicList;
            recipeService.addrecipe($scope.recipe, function (data, status) {
                if (status === 200) {
                    $scope.addRecipe.$setPristine();
                    $scope.addRecipe.$setValidity();
                    $scope.addRecipe.$setUntouched();
                    $scope.addRecipe.title.$setPristine();
                    $scope.addRecipe.title.$setValidity();
                    $scope.addRecipe.title.$setUntouched();
                    $scope.recipe = {};
                    $scope.input = null;
                    $scope.file = null;
                    $scope.attributes = angular.copy($rootScope.initAttributes);
                    $scope.bakingDishes = angular.copy($rootScope.initDish);
                    $scope.ingredients = [];
                    $scope.checkAttributes= [];
                    $scope.ingredientsList = [];
                    $scope.recipeDynamicList = [];
                    $scope.successMessage = data.msg;
                    $scope.successShow = true;
                    $scope.selectedItem = null;
                    $scope.selectedItem = null;
                    $scope.selectItem = undefined;
                    $scope.searchText = '';
                    $scope.$$childTail.searchText = '';
                    $scope.$broadcast('rebuild:me');
                    $rootScope.successMessage.push(data);
                    $timeout(function () {
                        angular.element(document.getElementsByClassName('scroll-overview')).css('top', '0px');
                    }, 100);
                } else{
                    $rootScope.errorMessage.push(data);
                }
            });
        };
        $scope.selected = function (index) {
            $scope.selectedDish = {
                type: $scope.initDish[index].dbValue,
                ui: []
            };
            $scope.valueChanged = function () {
                $scope.selectedDish.ui.length = 0;
                angular.forEach($scope.initDish[index].ui, function (v, k) {
                    $scope.selectedDish.ui.push({
                        value: v.value,
                        dbValue: v.dbValue
                    });
                });
            };
        };
        recipeService.getAll(function (data, status) {
            if (status === 200) {
                $scope.category = data.data;
                $scope.attributes = angular.copy($rootScope.initAttributes);
                $scope.weightUnit = angular.copy($rootScope.weigthUnitData);
                $scope.addrow();
            } else{
                $scope.errorMessage = data;
            }
        });
        subrecipeService.getAll(function (data, status) {
            if (status === 200) {
                $scope.subCategory = data.data;
            } else{
                $scope.errorMessage = data;
            }
        });
        $scope.selectItem = function (item) {
            var index = $scope.recipeDynamicList.length;
            if (item !== undefined){
                $scope.recipeDynamicList[index] = {
                    title: item.title,
                    id: item._id,
                    notes: $scope.notes,
                    ingredients:  item.ingredients
                };
                $scope.$broadcast('rebuild:me');
            }
        };
        $scope.showList = function (item) {
            $scope.listShow = true;
            $scope.ingredientsList = item.ingredients;
            $scope.ingredientsCopy = angular.copy(item.ingredients);
            $scope.$broadcast('rebuild:me');
        };
        $scope.seletectedCategory = function (item) {
            $scope.searchRecipe = item._id;
        };
        $scope.searchItem = function(searchText) {
            var s_data = {
                subRecipeCategory: $scope.searchRecipe,
                phrase: searchText
            };
            return $http.post(config.backend + config.subrecipe.recipelist, s_data, {ignoreLoadingBar: true})
                .then(function(results) {
                    return results.data.data;
                });
        };
        $scope.remove = function (index) {
            $scope.recipeDynamicList.splice(index, 1);
        };
        $scope.addrow = function (index) {
            $scope.ingredients.push(index);
        };
        $scope.draw = function () {
            $mdDialog.show({
                clickOutsideToClose : false,
                contentElement: '#myDialog',
                parent: angular.element(document.body)
            });
        };
        $scope.fabric = {};
        $scope.FabricConstants = FabricConstants;
        $scope.updateCanvas = function() {
            $scope.fabric.save();
            $mdDialog.cancel();
        };
        $scope.close = function () {
            $mdDialog.cancel();
            $scope.fabric.clearCanvas();
            $scope.fabric.setDirty(true);
        };
        $scope.init = function() {
            $scope.fabric = new Fabric({});
        };
        $scope.$on('canvas:created', $scope.init);
        $scope.myDropdownIsActive = function () {
            $scope.isActive = !$scope.isActive;
            $scope.$broadcast('rebuild:me');
        };
        $scope.pickedItem = function (item) {
            $scope.pushImage = function () {
                $scope.fabric.addImage(item);
                $scope.isActive = !$scope.isActive;
            };
        };
        $scope.createSub = function (id) {
            $mdDialog.show({
                locals:{itemId: id},
                controller: 'recipeCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/recipe/main/sub.create.html'
            }).then(function(data) {
                var item = data.data;
                var index = $scope.recipeDynamicList.length;
                if (data !== undefined){
                    $scope.recipeDynamicList[index] = {
                        title: item.title,
                        id: item._id,
                        ingredients:  item.ingredients
                    };
                }
                $scope.$broadcast('rebuild:me');
                // $mdDialog.hide();
            });
        };

        $scope.searchIngredients = function(searchIngr) {
            var s_data = {
                phrase: searchIngr
            };
            return $http.post(config.backend + config.ingredients.search, s_data, {ignoreLoadingBar: true})
                .then(function(results) {
                    return results.data.data;
                });
        };
        $scope.selectItemIngredients = function (ingredient, index) {
            if (ingredient !== undefined){
                $scope.ingredientsList[index] = {
                    title: ingredient.title,
                    weightUnit: ingredient.weightUnit,
                    weightValue: $scope.weightValue,
                    _id: ingredient._id,
                    notes: $scope.notes
                };
                $scope.ingredientsCopy = angular.copy($scope.ingredientsList);
            }
        };
        $scope.searchInputChange = function (searchIngr, index) {
            if (searchIngr !== undefined && searchIngr !== null && searchIngr !== ""){
                $scope.ingredientsList[index].title = searchIngr
            } else{
                $scope.ingredientsList[index].title = $scope.ingredientsCopy[index].title;
            }
        };
        $scope.removeIngredients = function (index) {
            $scope.ingredientsList.splice(index, 1);
        };
        $scope.addIngredients = function () {
            $scope.ingredientsList.push('');
            $scope.ingredientsCopy = angular.copy($scope.ingredientsList);
            $scope.$broadcast('rebuild:me');
        };
    }]);