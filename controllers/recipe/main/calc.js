recipeModule.controller('calcRecipeCtrl', ['$rootScope', '$scope', 'recipeService', '$stateParams', 'config', '$http',
    function($rootScope, $scope, recipeService, $stateParams, config, $http) {
        $scope.imgUrl = config.uploads;
        recipeService.getAll(function (data, status) {
            if (status === 200) {
                $scope.category = data.data;
            } else{
                $scope.errorMessage = data;
            }
        });
        $scope.seletectedCategory = function (item) {
            $scope.searchRecipe = item._id;
        };
        $scope.searchItem = function(searchText) {
            var s_data = {
                subRecipeCategory: $scope.searchRecipe,
                phrase: searchText
            };
            return $http.post(config.backend + config.subrecipe.recipelist, s_data)
                .then(function(results) {
                    return results.data.data;
                });
        };
        $scope.selectItem = function (item) {
            var index = $scope.recipeDynamicList.length;
            if (item !== undefined){
                $scope.recipeDynamicList[index] = {
                    title: item.title,
                    id: item._id,
                    notes: $scope.notes,
                    ingredients:  item.ingredients
                };
                $scope.$broadcast('rebuild:me');
            }
        };
    }]);