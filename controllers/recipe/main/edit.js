recipeModule.controller('editRecipeCtrl', ['$rootScope', '$scope', '$mdDialog', 'Upload', 'subrecipeService',
    'recipeService', 'ingredientsService', '$http', 'initData', 'config', '$timeout', 'Fabric', 'FabricConstants', '$stateParams',
    function($rootScope, $scope, $mdDialog, Upload, subrecipeService, recipeService, ingredientsService, $http, initData, config, $timeout, Fabric, FabricConstants, $stateParams) {
        $scope.subRecipeData = false;
        $scope.imgUrl = config.uploads;
        $scope.recipe = {};
        $scope.recipeDynamicList = [];
        $scope.ingredientsList = [];
        $scope.ingredients = [];
        $scope.checkAttributes = [];
        $scope.selectedDish = [];
        $scope.excludeItems = [];
        $scope.recipe.bakingDish = {};
        $scope.input = null;
        $scope.listShow = false;
        $scope.recipeimageurl = (config.backend + 'images/categories/');
        $scope.onChangeAttributes = function (v) {
            v.checkValue = !v.checkValue;
            if(v.checkValue){
                $scope.checkAttributes.push(v.value);
            } else {
                var index = $scope.checkAttributes.indexOf(v.value);
                $scope.checkAttributes.splice(index, 1);
            }
        };
        recipeService.editrecipe($stateParams.id, {ignoreLoadingBar: true}, function (data, status) {
            if (status === 200) {
                $scope.recipe = data.data;
                if(data.data.subRecipes !== undefined){
                    $scope.recipeDynamicList = data.data.subRecipes;
                    $scope.subRecipeData = true;
                }
                $scope.showList($scope.recipeDynamicList[0]);
                if(data.data.attributes !== undefined){
                    $scope.checkAttributes = data.data.attributes;
                }
                $scope.attributes = angular.copy($rootScope.initAttributes);
                angular.forEach($scope.attributes, function (v, k) {
                    v.checkValue = $scope.checkAttributes.indexOf(v.value) !== -1;
                });
                if(data.data.bakingDish !== undefined){
                    $scope.selectedType = data.data.bakingDish.type;
                } else {
                    $scope.isSelectedFalse = true;
                }
                $scope.weightUnit = angular.copy($rootScope.weigthUnitData);
                $scope.$broadcast('rebuild:me');
                $scope.imageUrl = data.data.picture;
                $scope.fabric.json = data.data.sketch;
            } else{
                $rootScope.errorMessage.push(data);
            }
        });
        $scope.$watch('recipe.bakingDish.type', function () {
            if($scope.recipe.bakingDish.type === $scope.selectedType){
                $scope.isSelected = true;
                $scope.isSelectedFalse = false;
            } else{
                $scope.isSelected = false;
                $scope.isSelectedFalse = true;
            }
        });
        $scope.$watch('file', function () {
            Upload.base64DataUrl($scope.file).then(function(url){
                $scope.imageUrl = url;
            });
        });
        $scope.clearImage = function() {
            $scope.file = null;
        };
        $scope.update = function () {
            angular.forEach($scope.recipeDynamicList, function (v, k) {
                $scope.recipeDynamicList[k].ingredients = $scope.recipeDynamicList[k].ingredients.filter(function (val) {
                    return val;
                });
            });
            $scope.recipe.picture = $scope.imageUrl;
            $scope.recipe.attributes = $scope.checkAttributes;
            if(($scope.recipe.bakingDish !== undefined) && ($scope.recipe.bakingDish.type !== $scope.selectedType)){
                $scope.recipe.bakingDish = $scope.selectedDish;
            }
            $scope.recipe.sketch = $scope.fabric.json;
            $scope.recipe.subRecipes = $scope.recipeDynamicList;
            recipeService.updaterecipe($stateParams.id, $scope.recipe, function (data, status) {
                if (status === 200) {
                    $scope.imageUrl = data.data.picture;
                    $rootScope.successMessage.push(data);
                    $scope.recipeDynamicList = data.data.subRecipes;
                    $scope.showList($scope.recipeDynamicList[0]);
                    $timeout(function () {
                        angular.element(document.getElementsByClassName('scroll-overview')).css('top', '0px');
                    }, 100);
                } else{
                    $rootScope.errorMessage.push(data);
                }
            });
        };
        $scope.addrow = function (index) {
            $scope.ingredients.push(index);
        };
        $scope.selected = function (index) {
            $scope.selectedDish = {
                type: $scope.initDish[index].dbValue,
                ui: []
            };
            $scope.valueChanged = function () {
                $scope.selectedDish.ui.length = 0;
                angular.forEach($scope.initDish[index].ui, function (v, k) {
                    $scope.selectedDish.ui.push({
                        value: v.value,
                        dbValue: v.dbValue
                    });
                });
            };
        };
        recipeService.getAll(function (data, status) {
            if (status === 200) {
                $scope.category = data.data;
                $scope.addrow();
            } else{
                $scope.errorMessage = data;
            }
        });
        subrecipeService.getAll(function (data, status) {
            if (status === 200) {
                $scope.subCategory = data.data;
            } else{
                $rootScope.errorMessage.push(data);
            }
        });
        $scope.selectItem = function (item) {
            var index = $scope.recipeDynamicList.length;
            if (item !== undefined){
                $scope.recipeDynamicList[index] = {
                    title: item.title,
                    id: item._id,
                    notes: $scope.notes,
                    ingredients:  item.ingredients
                };
                $scope.$broadcast('rebuild:me');
            }
        };
        $scope.showList = function (item) {
            $scope.listShow = true;
            $scope.ingredientsList = item.ingredients;
            $scope.ingredientsCopy = angular.copy(item.ingredients);
            $scope.$broadcast('rebuild:me');
        };
        $scope.seletectedCategory = function (item) {
            $scope.searchRecipe = item._id;
        };
        $scope.searchItem = function(searchText) {
            var s_data = {
                subRecipeCategory: $scope.searchRecipe,
                phrase: searchText
            };
            return $http.post(config.backend + config.subrecipe.recipelist, s_data, {ignoreLoadingBar: true})
                .then(function(results) {
                    return results.data.data;
                });
        };
        $scope.remove = function (index) {
            $scope.recipeDynamicList.splice(index, 1);
        };
        $scope.draw = function () {
            $mdDialog.show({
                clickOutsideToClose : false,
                contentElement: '#myDialog',
                parent: angular.element(document.body)
            });
            $scope.fabric.loadJSON($scope.fabric.json);
        };
        $scope.fabric = {};
        $scope.FabricConstants = FabricConstants;
        $scope.updateCanvas = function() {
            $scope.fabric.save();
            $mdDialog.hide();
        };
        $scope.init = function() {
            $scope.fabric = new Fabric({
                JSONExportProperties: FabricConstants.JSONExportProperties,
                textDefaults: FabricConstants.textDefaults,
                shapeDefaults: FabricConstants.shapeDefaults,
                json: $scope.fabric.json
            });
        };
        $scope.$on('canvas:created', $scope.init);
        $scope.createSub = function (id) {
            $mdDialog.show({
                locals:{itemId: id},
                controller: 'recipeCtrl',
                clickOutsideToClose : true,
                templateUrl: 'views/recipe/main/sub.create.html'
            }).then(function() {
                $mdDialog.hide();
            });
        };
        $scope.myDropdownIsActive = function () {
            $scope.isActive = !$scope.isActive;
            $scope.$broadcast('rebuild:me');
        };
        $scope.pickedItem = function (item) {
            $scope.pushImage = function () {
                $scope.fabric.addImage($scope.recipeimageurl + item  + '');
                $scope.isActive = !$scope.isActive;
            };
        };
        $scope.close = function () {
            $mdDialog.cancel();
            $scope.fabric.clearCanvas();
            $scope.fabric.setDirty(true);
        };
        $scope.searchIngredients = function(searchIngr) {
            var s_data = {
                phrase: searchIngr
            };
            return $http.post(config.backend + config.ingredients.search, s_data, {ignoreLoadingBar: true})
                .then(function(results) {
                    return results.data.data;
                });
        };
        $scope.selectItemIngredients = function (ingredient, index) {
            if (ingredient !== undefined){
                $scope.ingredientsList[index] = {
                    title: ingredient.title,
                    weightUnit: ingredient.weightUnit,
                    weightValue: $scope.weightValue,
                    _id: ingredient._id,
                    notes: $scope.notes
                };
                $scope.ingredientsCopy = angular.copy($scope.ingredientsList);
            }
        };
        $scope.searchInputChange = function (searchIngr, index) {
            if (searchIngr !== undefined && searchIngr !== null && searchIngr !== ""){
                $scope.ingredientsList[index].title = searchIngr
            } else{
                $scope.ingredientsList[index].title = $scope.ingredientsCopy[index].title;
            }
        };
        $scope.removeIngredients = function (index) {
            $scope.ingredientsList.splice(index, 1);
        };
        $scope.addIngredients = function () {
            $scope.ingredientsList.push('');
            $scope.ingredientsCopy = angular.copy($scope.ingredientsList);
            $scope.$broadcast('rebuild:me');
        };
    }]);
