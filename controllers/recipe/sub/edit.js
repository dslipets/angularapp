recipeModule.controller('editrecipeCtrl', ['$rootScope', '$scope', '$mdDialog', 'Upload', 'subrecipeService',
    'ingredientsService', '$http', 'initData', 'config', '$stateParams', '$timeout', '$filter',
    function($rootScope, $scope, $mdDialog, Upload, subrecipeService, ingredientsService, $http, initData, config, $stateParams, $timeout, $filter) {
        $rootScope.successShow = false;
        $rootScope.errorShow = false;
        $scope.hidden = false;
        $scope.selectedAttr = [];
        $scope.imgUrl = config.uploads;
        $scope.recipe = {};
        $scope.recipe.bakingDish = {};
        $scope.onChangeAttributes = function (v) {
            v.checkValue = !v.checkValue;
            if(v.checkValue){
                $scope.checkAttributes.push(v.value);
            } else {
                var index = $scope.checkAttributes.indexOf(v.value);
                $scope.checkAttributes.splice(index, 1);
            }
        };
        subrecipeService.editrecipe($stateParams.id, function (data, status) {
            if (status === 200) {
                $scope.recipe = data.data;
                $scope.ingredients = data.data.ingredients;
                $scope.checkAttributes = data.data.attributes;
                $scope.attributes = angular.copy($rootScope.initAttributes);
                $scope.ingredientsCopy = angular.copy($scope.ingredients);
                angular.forEach($scope.attributes, function (v, k) {
                    v.checkValue = $scope.checkAttributes.indexOf(v.value) !== -1;
                });
                if(data.data.bakingDish !== undefined){
                    $scope.selectedType = data.data.bakingDish.type;
                } else{
                    $scope.isSelectedFalse = true;
                }
                $scope.weightUnit = angular.copy($rootScope.weigthUnitData);
                $scope.imageUrl = data.data.picture;
            } else{
                $rootScope.errorMessage.push(data);
            }
        });
        $scope.$watch('recipe.bakingDish.type', function () {
            if($scope.recipe.bakingDish.type === $scope.selectedType){
                $scope.isSelected = true;
                $scope.isSelectedFalse = false;
            } else{
                $scope.isSelected = false;
                $scope.isSelectedFalse = true;
            }
        });
        $scope.$watch('file', function () {
            Upload.base64DataUrl($scope.file).then(function(url){
                $scope.imageUrl = url;
            });
        });
        $scope.clearImage = function() {
            $scope.file = null;
        };
        $scope.update = function () {
            $scope.recipe.picture = $scope.imageUrl;
            $scope.recipe.ingredients = $scope.ingredients.filter(function (val) {
                return val;
            });
            $scope.recipe.attributes = $scope.checkAttributes;
            if(($scope.recipe.bakingDish !== undefined) && ($scope.recipe.bakingDish.type !== $scope.selectedType)){
                $scope.recipe.bakingDish = $scope.selectedDish;
            }
            subrecipeService.update($stateParams.id, $scope.recipe, function (data, status) {
                if (status === 200) {
                    $scope.imageUrl = data.data.picture;
                    $rootScope.successMessage.push(data);
                    $scope.ingredients = data.data.ingredients;
                    $timeout(function () {
                        angular.element(document.getElementsByClassName('scroll-overview')).css('top', '0px');
                    }, 100);
                } else{
                    $rootScope.errorMessage.push(data);
                }
            });
        };
        subrecipeService.getAll(function (data, status) {
            if (status === 200) {
                $scope.category = data.data;
            } else{
                $scope.errorMessage = data;
            }
        });
        $scope.searchItem = function(searchText) {
            var s_data = {
                phrase: searchText
            };
            return $http.post(config.backend + config.ingredients.search, s_data)
                .then(function(results) {
                    return results.data.data;
                });
        };
        $scope.selectItem = function (item, index, searchText) {
            console.log(item);
            console.log(searchText);
            if (item !== undefined){
                $scope.ingredients[index] = {
                    title: item.title,
                    weightUnit: item.weightUnit,
                    weightValue: $scope.weightValue,
                    _id: item._id,
                    notes: $scope.notes,
                    line: $scope.check
                };
            }
        };
        $scope.searchInputChange = function (searchText, index) {
            if (searchText !== undefined && searchText !== null && searchText !== ""){
                $scope.ingredients[index].title = searchText
            } else{
                $scope.ingredients[index].title = $scope.ingredientsCopy[index].title;
            }
        };
        $scope.selected = function (index) {
            $scope.selectedDish = {
                type: $scope.initDish[index].dbValue,
                ui: []
            };
            $scope.valueChanged = function () {
                $scope.selectedDish.ui.length = 0;
                angular.forEach($scope.initDish[index].ui, function (v, k) {
                    $scope.selectedDish.ui.push({
                        value: v.value,
                        dbValue: v.dbValue
                    });
                });
            };
        };
        $scope.remove = function (index) {
            $scope.ingredients.splice(index, 1);
        };
        $scope.addrow = function (index) {
            $scope.ingredients.push(index);
        };
    }]);
