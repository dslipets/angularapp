recipeModule.controller('recipeCtrl', ['$rootScope', '$scope', '$mdDialog', 'Upload', 'subrecipeService',
    'ingredientsService', '$http', 'initData', 'config', '$state', '$timeout', '$filter',
    function($rootScope, $scope, $mdDialog, Upload, subrecipeService, ingredientsService, $http, initData, config, $state, $timeout, $filter) {
        $rootScope.successShow = false;
        $rootScope.errorShow = false;
        $scope.attributes = [];
        $scope.recipe = {};
        $scope.ingredients = [];
        $scope.selectedDish = [];
        $scope.recipe.bakingDish= {};
        $scope.input = null;
        $scope.checkAttributes = [];
        $scope.$watch('file', function () {
            Upload.base64DataUrl($scope.file).then(function(url){
                $scope.subimageUrl = url;
            });
        });
        $scope.clearImage = function() {
            $scope.file = null;
        };
        $scope.onChangeAttributes = function (v) {
            v.checkValue = !v.checkValue;
            if(v.checkValue){
                $scope.checkAttributes.push(v.value);
            } else {
                var index = $scope.checkAttributes.indexOf(v.value);
                $scope.checkAttributes.splice(index, 1);
            }
        };
        $scope.create = function () {
            $scope.recipe.picture = $scope.subimageUrl;
            $scope.recipe.ingredients = $scope.ingredients.filter(function (val) {
                return val;
            });
            $scope.recipe.attributes = $scope.checkAttributes;
            if($scope.recipe.bakingDish.type !== undefined){
                $scope.recipe.bakingDish = $scope.selectedDish;
            }
            subrecipeService.addrecipe($scope.recipe, function (data, status) {
                if (status === 200) {
                    if($state.is('SubRecipe')){
                        $scope.addRecipe.$setPristine();
                        $scope.addRecipe.$setValidity();
                        $scope.addRecipe.$setUntouched();
                        $scope.addRecipe.title.$setPristine();
                        $scope.addRecipe.title.$setValidity();
                        $scope.addRecipe.title.$setUntouched();
                        $scope.recipe = {};
                        $scope.input = null;
                        $scope.file = null;
                        $scope.attributes = angular.copy($rootScope.initAttributes);
                        $scope.bakingDishes = angular.copy($rootScope.initDish);
                        $scope.ingredients = [];
                        $scope.checkAttributes = [];
                        $rootScope.successMessage.push(data);
                        $timeout(function () {
                            $scope.addrow();
                            angular.element(document.getElementsByClassName('scroll-overview')).css('top', '0px');
                        }, 100);
                        $timeout( function() { $scope.$$childHead.$mdAutocompleteCtrl.clear(); },100);
                    } else {
                        $mdDialog.hide(data);
                    }
                } else{
                    $rootScope.errorMessage.push(data);
                }
            });
        };
        $scope.selected = function (index) {
            $scope.selectedDish = {
                type: $scope.bakingDishes[index].dbValue,
                ui: []
            };
            $scope.valueChanged = function () {
                $scope.selectedDish.ui.length = 0;
                angular.forEach($scope.bakingDishes[index].ui, function (v, k) {
                    $scope.selectedDish.ui.push({
                        value: v.value,
                        dbValue: v.dbValue
                    });
                });
            };
        };
        subrecipeService.getAll(function (data, status) {
            if (status === 200) {
                $scope.category = data.data;
                $scope.attributes = angular.copy($rootScope.initAttributes);
                $scope.bakingDishes = angular.copy($rootScope.initDish);
                $scope.weightUnit = angular.copy($rootScope.weigthUnitData);
                $scope.addrow();
            } else{
                $scope.errorMessage = data;
            }
        });
        $scope.searchItem = function(searchText) {
            var s_data = {
                phrase: searchText
            };
            return $http.post(config.backend + config.ingredients.search, s_data, {ignoreLoadingBar: true})
                .then(function(results) {
                    return results.data.data;
                });
        };
        $scope.selectItem = function (item, index, searchText) {
            if (item !== undefined){
                $scope.ingredients[index] = {
                    title: item.title,
                    weightUnit: item.weightUnit,
                    weightValue: $scope.weightValue,
                    _id: item._id,
                    notes: $scope.notes,
                    line: $scope.check
                };
                $scope.ingredientsCopy = angular.copy($scope.ingredients);
            }
        };
        $scope.searchInputChange = function (searchText, index) {
            if (searchText !== undefined && searchText !== null && searchText !== ""){
                $scope.ingredients[index].title = searchText
            } else if($scope.ingredientsCopy[index].title !== undefined){
                $scope.ingredients[index].title = $scope.ingredientsCopy[index].title;
            }
        };
        $scope.remove = function (index) {
            $scope.ingredients.splice(index, 1);
        };
        $scope.addrow = function (index) {
            $scope.ingredients.push("");
            $scope.ingredientsCopy = angular.copy($scope.ingredients);
        };
        $scope.close = function() {
            $mdDialog.cancel();
        };
    }]);