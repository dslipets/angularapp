recipeModule.controller('viewSubRecipeCtrl', ['$rootScope', '$scope', 'subrecipeService', 'Fabric', 'FabricConstants', '$stateParams', 'config',
    function($rootScope, $scope, subrecipeService, Fabric, FabricConstants, $stateParams, config) {
        $scope.imgUrl = config.uploads;
        $scope.multiplier = null;
        subrecipeService.editrecipe($stateParams.id, function (data, status) {
            if (status === 200) {
                $scope.recipe = data.data;
                subrecipeService.view(data.data.subRecipeCategory, function (data, status) {
                    if (status === 200) {
                        $scope.category = data.data;
                    } else {
                    }
                });
            } else{
                $scope.errorMessage = data;
            }
        });
    }]);