var isTest = true;
var srvUrl = isTest ? 'localhost' : 'bakeoffice.uvision.co.il';

angular.module('configuration', []).value('config', {
    backend: 'http://'+srvUrl+':9000/',
    uploads: 'http://'+srvUrl+':9000/images/uploads/',
    url: {
        auth: 'auth/local'
    },
    init : 'api/init',
    users: {
        me: 'api/users/me',
        business: 'business',
        profile: 'api/business',
        payments: 'api/payments/'
    },
    ingredients: {
        list: 'api/ingrediants',
        search: 'api/ingrediants/search/'
    },
    subrecipe: {
        list: 'api/subRecipeCategories',
        search: 'api/subRecipeCategories/search',
        add: 'api/subRecipes',
        edit: 'api/subRecipes',
        cat_share: 'api/subRecipeCategories/share/',
        share: 'api/subRecipes/share/',
        recipelist: 'api/subRecipes/search'
    },
    recipe: {
        list: 'api/recipeCategories',
        search: 'api/recipeCategories/search',
        add: 'api/recipes',
        edit: 'api/recipes',
        cat_share: 'api/recipeCategories/share/',
        share: 'api/recipes/share/',
        recipelist: 'api/recipes/search'
    },
    customer: {
        crud: 'api/customers',
        search: 'api/customers/search'
    },
    calendar: {
        crud: 'api/orders',
        search: 'api/orders/search'
    }
});